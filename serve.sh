#!/bin/sh
hugo serve \
	--baseURL http://liam.bobcob7.com \
	--bind 0.0.0.0 \
	--buildDrafts \
	--buildFuture
