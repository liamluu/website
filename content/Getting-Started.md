---
title: "Getting Started with Hugo"
date: 2019-12-03T22:29:11Z
draft: true
description: "Creating Your First Post with Hugo"
type: "post"
image: "images/getting-started/bwbowl.jpg"
categories: 
  - "General"
tags:
  - "Test"
---

For a nice cheatsheet on how to use MarkDown please go [here](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).

Here are the basic steps for creating a post:
1 Open your project in VS Code. Make sure that hugo is installed.
+ Create you content with the `hugo new {PAGE NAME}.md` where {PAGE NAME} is a short title of your page. This shouldn't have any spaces instead separate-the-words-with-dashes.
+  Open your new file in `content/{PAGE NAME}.md`
+ You can change your title and description at the top section of the page. This section contains all of the metadata for your website.
+ Every page should have a thumbnail. Drag a picture that you want to be your thumbnail into `static/images`. Then you can reference this in your metatdata section with `images/{IMAGE NAME}`.
+ Fill in your categories and tags.
+ Below the three dashes `---` start writing your content with markdown. Again, refer to [this guide](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).
+ Once you are ready, you can test the site locally, with `hugo serve` or you can just publish it. Be sure to turn the `draft` option to `false` in the metadata section.
+ To publish. Go over to the Source Control section on VS code. Stage all of your changes, write a short commit message, then press the check mark. Then you're ready to upload, just press the three dots on the Source Control section and select `Push`.

---

